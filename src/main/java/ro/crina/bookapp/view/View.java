package ro.crina.bookapp.view;

import java.util.Scanner;

public class View {

     public void run(){

        boolean exit = false;
        while(!exit){
            showMenu();
            //read user selection
            int userSelection = readSelection();
            //process user selection
            switch(userSelection){
                case 1:
                    select();
                    break;
                case 2:
                    selectBook();
                    break;
                case 3:
                    exit = true;
                    System.out.println("Bye Bye curule");
                    break;
                default:
                    System.out.println("The selection " + userSelection + " is not permitted");
            }
        }
    }

    public void select(){
        boolean back = false;
        while (!back){
            authorSubMenu();
            int subMenuSelection = readSelection();
            switch (subMenuSelection){
                case 1:
                    System.out.println("un autor de literatura");
                    break;
                case 2:
                    System.out.println("un autor de beletristica");
                    break;
                case 3:
                    System.out.println("un autor de carti stintifice");
                    break;
                case 4:
                    back = true;
                    System.out.println("Bine ai revenit la meniu");
                    break;
                default:
                    System.out.println("The selection " + subMenuSelection + " is not permitted");

            }

        }
    }

    public void selectBook(){

        boolean back = false;
        while (!back){
            booksSubMenu();
            int bookSubMenuSelection = readSelection();
            switch (bookSubMenuSelection){
                case 1:
                    System.out.println("o carte de literatura");
                    break;
                case 2:
                    System.out.println("o carte de beletristica");
                    break;
                case 3:
                    System.out.println("o carte stiintifica");
                    break;
                case 4:
                    back = true;
                    System.out.println("Bine ai revenit la meniu");
                    break;
                default:
                 System.out.println("The selection " + bookSubMenuSelection + " is not permitted");
                  booksSubMenu();
            }
        }

    }

        private int readSelection(){
        Scanner inputReader = new Scanner(System.in);
        return inputReader.nextInt();
    }

    private void showMenu(){
        System.out.println("Select: ");
        System.out.println("1.Authors ");
        System.out.println("2.Books");
        System.out.println("3.Exit ");
    }

    private void authorSubMenu(){

        System.out.println("1. View authors ");
        System.out.println("2. Add author ");
        System.out.println("3. Delete author ");
        System.out.println("4. Back");

    }

    private void booksSubMenu(){
        System.out.println("1. View books ");
        System.out.println("2. Add book ");
        System.out.println("3. Delete book ");
        System.out.println("4. Back");
    }

}
