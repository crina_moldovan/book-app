package ro.crina.bookapp;

import ro.crina.bookapp.view.View;

/**
 * Hello world!
 *
 */
public class App {

    public static void main( String[] args ) {
        View ui = new View();
        ui.run();
    }
}
